import React from "react";




//Create a context object

// Context object is a different approach in passing information between components and allows easier access by avoiding props-drilling
//type of object that can be used to store information that can be shared to other components within the app
const UserContext = React.createContext();

// the provider component allows other componens to consume or use the context object and supply the necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;