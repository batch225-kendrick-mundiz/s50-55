import React from "react";



export default function PageNotFound(){
    return(
        <>
            <h1>Page not found</h1>
            <p>go back to <a href="/">Homepage</a></p>
        </>
        
    )
}