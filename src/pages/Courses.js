import CoursesData from "../data/CoursesData";
import CourseCard from "../components/CoursesCard"

const course = CoursesData.map(course => {
    return (
        <CourseCard key={course.id} courseProp={course} />
    )
})

export default function Courses(){
    //console.log(CoursesData[0]);
    return (
        <>
            {course}
        </>
    )
}