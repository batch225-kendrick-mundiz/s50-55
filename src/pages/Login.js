// Sets the global user state to have properties obtained from local storage.
        // setUser({email: localStorage.getItem('email')});
// {} - if packages or component
import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom'

// file - do not use {} if you want to import
import UserContext from '../UserContext'
 
export default function Login() {


    const { user, setUser } = useContext(UserContext)

            const [email, setEmail] = useState("");
            const [password, setPassword] = useState("");
 

            useEffect(() => {
                if (email !== '' && password !== ''){
                    console.log(email + password);
                }
            }, [email, password])

            function authenticate(e) {
                e.preventDefault();

				localStorage.setItem('email', email);

                setEmail("");
                setPassword("");
				// navigate("/")

				// Sets the global user state to have properties obtained from local storage.
				setUser({email: localStorage.getItem('email')});


                alert("Logged in");
            }

		    return (
				(!!user.email) ?
            	<Navigate to="/courses"/>
        		:
		        <Form onSubmit={(e) => authenticate(e)} className="col-4">
		            <Form.Group controlId="userEmail" >
		                <Form.Label>Email address</Form.Label>
		                <Form.Control 
			                type="email" 
			                placeholder="Enter email" 
			                required
                            value={email}
                            onChange={e => setEmail(e.target.value)}
		                />
		                <Form.Text className="text-muted">
		                    We'll never share your email with anyone else.
		                </Form.Text>
		            </Form.Group>

		            <Form.Group controlId="password">
		                <Form.Label>Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Password" 
			                required
                            value={password}
                            onChange={e => setPassword(e.target.value)}
		                />
		            </Form.Group>

                    <Button variant="success" type="submit" id="submitBtn">
		            	Login
		            </Button> 
		        </Form>
		    )

}