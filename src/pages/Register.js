import { Form, Button, Nav } from 'react-bootstrap';
import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext'

export default function Register() {

	const { user, setUser } = useContext(UserContext)

            const [isActive, setIsActive] = useState(false);
            const [email, setEmail] = useState("");
            const [password1, setPassword1] = useState("");
            const [password2, setPassword2] = useState("");

            useEffect(() => {
                if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
                    setIsActive(true);
                    console.log(email + password1 + isActive);
                }else {
                    setIsActive(false)
                }
            }, [email, password1, password2])

            function registerUser(e) {
                e.preventDefault();
                setIsActive(true);
                setEmail("");
                setPassword1("");
                setPassword2("");
				<Navigate to="/Login"/>
                alert("Registration Complete");
            }

		    return (
				(!!user.email) ?
            	<Navigate to="/courses"/>
        		:
		        <Form onSubmit={(e) => registerUser(e)}>
		            <Form.Group controlId="userEmail">
		                <Form.Label>Email address</Form.Label>
		                <Form.Control 
			                type="email" 
			                placeholder="Enter email" 
			                required
                            value={email}
                            onChange={e => setEmail(e.target.value)}
		                />
		                <Form.Text className="text-muted">
		                    We'll never share your email with anyone else.
		                </Form.Text>
		            </Form.Group>

		            <Form.Group controlId="password1">
		                <Form.Label>Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Password" 
			                required
                            value={password1}
                            onChange={e => setPassword1(e.target.value)}
		                />
		            </Form.Group>

		            <Form.Group controlId="password2">
		                <Form.Label>Verify Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Verify Password" 
			                required
                            value={password2}
                            onChange={e => setPassword2(e.target.value)}
		                />
		            </Form.Group>
                    
                    <Button variant="primary" type="submit" id="submitBtn" >
		            	Submit
		            </Button> 
                   
		            
		        </Form>
		    )

}