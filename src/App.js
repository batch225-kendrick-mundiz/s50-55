//import logo from './logo.svg';
import './App.css';
import AppNavbar from "./components/AppNavbar";
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { useState } from 'react';
import { UserProvider } from './UserContext';


function App() {


  const [ user, setUser ] = useState({
    email: localStorage.getItem('email')
  });

  const unsetUser = () => {
    localStorage.clear()
    setUser('')
  }
  return (
    // "UserProvider" component from "UserContext" and wrap all components in the app within it to allow re-rendering when the "value" prop changes.
    // Storing information in a context object is done by providing the information using the corresponding "Provider" component and passing the information via the "value" prop.
    // All information provided to the Provider component can be accessed later on from the context object as properties. 
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
    <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />}/>
            <Route path="/courses" exact element={<Courses />}/>
            <Route path="/register" exact element={<Register/>}/>
            <Route path="/login" exact element={<Login />}/>
            <Route path="/logout" exact element={<Logout />}/>
            <Route path="*" element={<PageNotFound />}></Route>
          </Routes>
        </Container>
    </Router>
    </UserProvider>
    </>
  );
}



export default App;
