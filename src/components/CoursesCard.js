import { useState, useEffect } from 'react';
import {Button, Card, Row, Col} from 'react-bootstrap';




export default function Courses(props){

	// check if the data was passed successfully
	// console.log(props.courseProp.name);
	// every componen recieves information in a form of an objec
	// console.log(typeof props)

	//const [getter, setter] = useState(initialgettervalue);

	
	const [counter, setCounter] = useState(0);
	const [slots, setSlots] = useState(30);
	const[isOpen, setIsOpen] = useState(true)
	// useEffect(() => {
    // 	setCounter(100);
  	// }, []);

	function enroll() {
		if(counter < 30){
			setCounter(counter + 1);
			setSlots(slots -1);
		}
	}

	useEffect(() => {
		if (slots === 0){
			setIsOpen(false)
			alert("Slots full")
			document.querySelector("#btnEnroll").disabled = true;
		}
	}, [slots])

    return (
        			    <Row>
			            <Col>
			                <Card className="cardHighlight p-3 align-items-start">
			                    <Card.Body>
			                        <Card.Title>
			                            <h2>{props.courseProp.name}</h2>
			                        </Card.Title>
                                    <Card.Text>
                                        Description:
			                            {props.courseProp.description}
			                        </Card.Text>
			                        <Card.Text>
			                           	Price:
			                            Php {props.courseProp.price}
			                        </Card.Text>
									<Card.Text>
										Enrollees: {counter}
									</Card.Text>
									<Card.Text>
										Slots: {slots}
									</Card.Text>
                                    <Button id="btnEnroll" onClick={enroll}>Enroll now</Button>
			                    </Card.Body>
			                </Card>
			            </Col>
			            </Row>
    )
}